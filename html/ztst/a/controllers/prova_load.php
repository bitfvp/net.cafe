<?php
//gerado pelo geracode
function fncprovalist(){
    $sql = "SELECT * FROM ztst_provas ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $provalista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $provalista;
}

function fncgetprova($id){
    $sql = "SELECT * FROM ztst_provas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getztst_provas = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getztst_provas;
}
?>