<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>

<div class="container">
    <?php
    include_once("../includes/ztst_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h2>HISTÓRICO DE LOTES</h2>
            <h4>FORNECEDOR: <strong><?php echo strtoupper($pessoa['nome']); ?></strong></h4>
            <h5>
            </h5>
            <h5>
                CPF: <strong>
                    <?php
                    if($pessoa['cpf']!="" and $pessoa['cpf']!=0) {
                        echo "<span class='text-info'>";
                        echo mask($pessoa['cpf'],'###.###.###-##');
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?>
                </strong>
                CNPJ: <strong>
                    <?php
                    if($pessoa['cnpj']!="" and $pessoa['cnpj']!=0) {
                        echo "<span class='text-info'>";
                        echo mask($pessoa['cnpj'],'##.###.###/####-##');
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?>
                </strong>
                TELEFONE: <strong>
                    <?php
                    if($pessoa['telefone']!="") {
                        echo "<span class='text-info'>";
                        echo $pessoa['telefone'];
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?>
                </strong>
            </h5>

        </div>
    </div>


    <hr class="hrgrosso">
    <?php
    try{
        $sql = "SELECT "
            . "ztst_entradas_lotes.id, "
            . "ztst_entradas.id as id_e, "
            . "ztst_entradas.romaneio, "
            . "ztst_entradas_lotes.data_ts, "
            . "ztst_entradas_lotes.letra, "
            . "ztst_entradas_lotes.tipo_cafe, "
            . "ztst_entradas_lotes.verificado, "
            . "ztst_entradas_lotes.peso_entrada, "
            . "ztst_entradas_lotes.bags_entrada, "
            . "ztst_entradas_lotes.localizacao, "
            . "ztst_entradas_lotes.localizacao_obs, "
            . "ztst_entradas_lotes.p_bo, "
            . "ztst_entradas_lotes.bo, "
            . "ztst_entradas_lotes.peso_atual, "
            . "ztst_entradas_lotes.bags_atual, "
            . "ztst_entradas_lotes.obs, "
            . "ztst_entradas.romaneio_tipo, "
            . "ztst_pessoas.nome AS fornecedor "
            . "FROM "
            . "ztst_entradas_lotes "
            . "INNER JOIN ztst_entradas ON ztst_entradas_lotes.romaneio = ztst_entradas.id "
            . "INNER JOIN ztst_pessoas ON ztst_entradas.fornecedor = ztst_pessoas.id ";
        $sql_where = "WHERE ztst_entradas_lotes.id>0 and ztst_entradas.fornecedor={$_GET['id']} ";
        $sql_orderby= "ORDER BY ztst_entradas_lotes.data_ts DESC LIMIT 0,1000";
        global $pdo;
        $consulta=$pdo->prepare($sql.$sql_where.$sql_orderby);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $lotes = $consulta->fetchAll();
    $lotes_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>

    <table class="table table-striped table-hover table-sm">
        <thead class="thead-dark">
        <tr>
            <th scope="col"><small>LOTE</small></th>
            <th scope="col"><small>DATA</small></th>
            <th scope="col"><small>FORNECEDOR</small></th>
            <th scope="col"><small>PRODUTO</small></th>
            <th scope="col"><small>PESO ENTRADA</small></th>
            <th scope="col"><small>PESO ATUAL</small></th>
            <th scope="col"><small>LOCALIZAÇÃO</small></th>
            <th scope="col"><small>B.O.</small></th>
        </tr>
        </thead>

        <?php

        // vamos criar a visualização,
        foreach ($lotes as $dados){
        $cordalinha = " font-weight-bold ";
        if ($dados['peso_atual']==null or $dados['peso_atual']==0 or $dados['peso_atual']==""){
            $cordalinha = " font-weight-lighter ";
        }else{
            if ($dados['p_bo']==1){
                $cordalinha = " font-weight-lighter font-italic ";
            }
        }

        $id_l = $dados["id"];
        $romaneio = $dados["romaneio"];
        $romaneio_tipo=fncgetromaneiotipo($dados["romaneio_tipo"]);

        $letra=fncgetletra($dados["letra"]);

        $entrada_id = $dados["id_e"];
        $data_l = $dados["data_ts"];

        $tipo_cafe = $dados["tipo_cafe"];
        $tipo_cafe_entrada = $dados["tipo_cafe_entrada"];
        if ($dados["verificado"]==0){
            $bebida="<small class='text-danger'>NÃO VERIFICADA</small>";
        }else{
            $bebida="<small class='text-success'>VERIFICADA</small>";
        }
        $peso_entrada = $dados["peso_entrada"];
        $sacas_entrada=$peso_entrada/60;
        $sacas_entrada=number_format($sacas_entrada, 1, '.', ',');
        $peso_entrada.=" Kg";
        $bags_entrada = $dados["bags_entrada"];
        $localizacao = $dados["localizacao"];
        $localizacao_obs = $dados["localizacao_obs"];
        $p_bo = $dados["p_bo"];
        $bo = $dados["bo"];
        $peso_atual = $dados["peso_atual"];
        $sacas_atual=$peso_atual/60;
        $sacas_atual=number_format($sacas_atual, 2, '.', ',');
        $peso_atual.=" Kg";
        $bags_atual = $dados["bags_atual"];
        $obs = $dados["obs"];
        $fornecedor = strtoupper($dados["fornecedor"]);

        ?>
        <tbody>
        <tr class="small <?php echo $cordalinha; ?>" id='<?php echo $romaneio_tipo; ?>'>
            <th scope="row" id="<?php echo $id_l;  ?>" style="white-space: nowrap;">
                <?php echo $romaneio_tipo.$romaneio." ".$letra; ?>
            </th>
            <td title="<?php echo datahoraBanco2data($dados["data_ts"]);?>">
                <?php echo dataRetiraHora($dados["data_ts"]); ?>
            </th>
            <td>
                <?php echo $fornecedor; ?>
            </td>
            <td>
                <?php echo fncgetprodutos($tipo_cafe)['abrev'];?>
            </td>

            <td>
                <?php echo $peso_entrada. ", ".$sacas_entrada."v";?>
            </td>

            <td>
                <?php echo $peso_atual. ", ".$sacas_atual."v";?>
            </td>

            <td>
                <?php echo fncgetlocal($localizacao)['nome']. " ". $localizacao_obs;?>
            </td>

            <td>
                <?php echo $bo;?>
            </td>
        </tr>
        <?php
        }
        ?>
        </tbody>
    </table>


    <br>




    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Usuário: <strong><?php echo fncgetusuario($_SESSION['id'])['nome']." ".date('d/m/Y H:i:s'); ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>