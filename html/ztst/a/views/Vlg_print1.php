<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vct_lista");
    exit();
}
?>

<div class="container">
    <?php
    include_once("../includes/ztst_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h2>CONTROLE DE LIGA INTERNA</h2>
            <h4><strong><?php echo datahoraBanco2data($entrada['data_ts']); ?></strong></h4>

        </div>
        <div class="col-4 text-right">
            <h4 class="">ROMANEIO: <strong>LG-<?php echo $entrada['romaneio']; ?></strong></h4>
<!--            <h4>NOTA: <strong>--><?php //echo $entrada['nota']; ?><!--</strong></h4>-->
        </div>
    </div>


    <hr class="hrgrosso">
    <h3>Seleções para processamento</h3>
    <?php
    //1 venda
    //2ct
    //3 devolucao
    try{
        $sql = "SELECT * FROM "
            ."ztst_entradas_lotes_saidas "
            ."WHERE id_destino=:id_destino and tipo_saida=4 "
            ."order by ztst_entradas_lotes_saidas.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":id_destino", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $saidas = $consulta->fetchAll();
    $saidas_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-hover table-striped">
                <thead class="thead-inverse thead-dark">
                <tr>
                    <th>DATA</th>
                    <th>PRODUTO</th>
                    <th>CATA</th>
                    <th>LOTE</th>
                    <th>FORNECEDOR</th>
                    <th>PESO</th>
                    <th>VALOR</th>
                    <th>SUBTOTAL</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $peso_total=0;
                $totalvalor=0;
                foreach ($saidas as $dados){
                    $peso_entrada = mask($dados["peso_entrada"],'######')." Kg";
                    $obs_entrada = $dados["obs_entrada"];
                    $peso_saida = mask($dados["peso_saida"],'######')." Kg";
                    $obs_saida = $dados["obs_saida"];

                    $data_ts = dataRetiraHora($dados["data_ts"]);
                    $tipo_cafe = fncgetprodutos($dados["tipo_cafe"])['abrev'];
                    $lote = fncgetlote($dados["lote"]);
                    $entrada=fncgetentrada($lote["romaneio"]);
                    $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

                    $letra=fncgetletra($lote["letra"]);

                    $loteinfo=$romaneio_tipo.$entrada['romaneio']." ".$letra;
                    $fornecedor = fncgetpessoa($entrada['fornecedor'])['nome'];

                    $peso_total=$peso_total+$dados["peso"];
                    $peso = $dados["peso"];
                    $sacas=$peso/60;
                    $sacas=number_format($sacas, 2, '.', ',');

                    $origem = fncgetlocal($dados["origem"])['nome'];
                    $p_bo = $dados["p_bo"];
                    $bo = $dados["bo"];

                    if ($lote["cata"]>0){
                        $cata=$lote["cata"]."%";
                    }else{
                        $cata="N.D.";
                    }

                    $valor_saca=$lote["valor_saca"];
                    $subtotalvalor=$valor_saca*($peso/60);
                    $totalvalor=$totalvalor+$subtotalvalor;
                    ?>

                    <tr id="" class="">
                        <td><?php echo $data_ts ?></td>
                        <td><?php echo $tipo_cafe; ?></td>
                        <td><?php echo $cata; ?></td>
                        <td><?php echo $loteinfo; ?></td>
                        <td><?php echo strtoupper($fornecedor); ?></td>
                        <td><?php echo $peso. "KG<br>".$sacas." volumes";?></td>
                        <td style='white-space: nowrap;'> R$ <?php echo number_format($valor_saca,2);?></td>
                        <td style='white-space: nowrap;'> R$ <?php echo number_format($subtotalvalor,2);?></td>
                    </tr>
                    <?php
                }
                $sacastotais=$peso_total/60;

                $mediavalor=@($totalvalor/$sacastotais);
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-6 border">
            PESAGEM LÍQUIDO TOTAL DOS LOTES: <strong><?php echo number_format($peso_total,2,',','')." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo number_format($sacastotais,2,',','.'); ?></strong>
        </div>
        <div class="col-6 border">
            MÉDIA DE VALOR POR VOLUME: R$ <strong><?php echo number_format($mediavalor,2,',','.'); ?></strong>
        </div>
        <div class="col-6 border">
            VALOR TOTAL: <strong>  R$ <?php echo number_format($totalvalor,2,',','.'); ?></strong>
        </div>
    </div>

    <br>
    <hr class="hrgrosso">
    <h3>Lotes gerados apartir de processo</h3>
    <?php
    //1 venda
    //2ct
    //3 devolucao
    try{
        $sql = "SELECT * \n"
            . "FROM ztst_entradas_lotes \n"
            . "WHERE (((ztst_entradas_lotes.romaneio)=:id_destino) and status=1)\n"
            . "ORDER BY ztst_entradas_lotes.data_ts ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":id_destino", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $lotes = $consulta->fetchAll();
    $lotes_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-hover table-striped">
                <thead class="thead-inverse thead-dark">
                <tr>
                    <th scope="col">LOTE</th>
                    <th scope="col">PRODUTO</th>
                    <th scope="col">CATA</th>
                    <th scope="col">FORNECEDOR</th>
                    <th scope="col">PESO INICIAL</th>
                    <th>VALOR</th>
                    <th>SUBTOTAL</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $peso_total=0;
                foreach ($lotes as $dados){
                    $entrada=fncgetentrada($dados['romaneio']);
                    $fornecedor = fncgetpessoa($entrada["fornecedor"])['nome'];
                    $romaneio = $entrada["romaneio"];
                    $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

                    $letra=fncgetletra($dados["letra"]);

                    $tipo_cafe = $dados["tipo_cafe"];
                    $id_e = $dados["id"];

                    $peso_entrada = $dados["peso_entrada"];
                    $sacas_entrada=$peso_entrada/60;

                    $peso_total=$peso_total+$dados["peso_entrada"];

                    if ($dados["cata"]>0){
                        $cata=$dados["cata"]."%";
                    }else{
                        $cata="N.D.";
                    }
                    $valor_saca=$dados["valor_saca"];
                    $subtotalvalor=$valor_saca*($peso_entrada/60);
                    $totalvalor=$totalvalor+$subtotalvalor;
                    ?>

                    <tr class="">
                        <th scope="row" id="">
                                <?php
                                    echo $romaneio_tipo.$romaneio." ".$letra;
                                ?>
                        </th>
                        <td class="small">
                            <?php echo fncgetprodutos($tipo_cafe)['nome'];?>
                        </td>
                        <td><?php echo $cata; ?></td>
                        <td>
                            <?php
                                echo strtoupper($fornecedor);
                            ?>
                        </td>

                        <td>
                            <?php echo $peso_entrada. "KG ou ".$sacas_entrada." volumes";?>
                        </td>


                        <td style='white-space: nowrap;'> R$ <?php echo number_format($valor_saca,2,',',' ');?></td>
                        <td style='white-space: nowrap;'> R$ <?php echo number_format($subtotalvalor,2,',',' ');?></td>
                    </tr>
                    <?php
                }
                $sacastotais=$peso_total/60;
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-6 border">
            PESAGEM LÍQUIDO TOTAL DOS LOTES: <strong><?php echo $peso_total." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo number_format($sacastotais,2,',',' '); ?></strong>
        </div>
    </div>

    <br>




    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Operador: <strong><?php echo fncgetusuario($_SESSION['id'])['nome']." ".date('d/m/Y H:i:s'); ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    // window.print()
</SCRIPT>