<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatório de estoque-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe

    $sql = "SELECT * FROM ztst_entradas_lotes WHERE ztst_entradas_lotes.peso_atual > 0 ORDER BY ";
    switch ($_GET['order']){
        case "local":
            $sql .= "ztst_entradas_lotes.localizacao ASC, ztst_entradas_lotes.data_ts ASC";
            break;
        case "tipo":
            $sql .= "ztst_entradas_lotes.tipo_cafe ASC, ztst_entradas_lotes.data_ts ASC";
            break;
        case "data":
            $sql .= "ztst_entradas_lotes.data_ts ASC";
            break;
    }
    global $pdo;
    $consulta = $pdo->prepare($sql);

    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $lotes = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>
<div class="container-fluid">
    <?php
    switch ($_GET['order']){
        case "local":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR LOCALIZAÇÃO, DATA DE ENTRADA</h3>";
            break;
        case "tipo":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR TIPO DE PRODUTO, DATA DE ENTRADA</h3>";
            break;
        case "data":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR DATA DE ENTRADA</h3>";
            break;
    }
    ?>

<!--    <h5>--><?php //echo fncgetsetor($setor)['setor']?><!--</h5>-->
    <h5><?php echo datahoraBanco2data(dataNow());?></h5>

    <table class="table table-bordered table-hover table-sm">
        <thead>
        <th>PRODUTO</th>
        <th>CATA</th>
        <th>FORNECEDOR</th>
        <th>LOTE</th>
        <th>VOLUMES</th>
        <th>VALOR/SACA</th>
        <th>BO</th>

        </thead>
        <tbody>
        <?php
        $infovolumes=0;
        $infovalor=0;
        foreach ($lotes as $lote){
            $entrada=fncgetentrada($lote['romaneio']);
            $cordalinha = "  ";
                if ($lote['p_bo']==1){
                    $cordalinha = " text-dark bg-warning ";
                }
            $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

            $letra=fncgetletra($lote["letra"]);

            if ($lote["cata"]>0){
                $cata=$lote["cata"]."%";
            }else{
                $cata="N.D.";
            }

            $valor_saca = $lote["valor_saca"];

            $peso_atual = $lote["peso_atual"];
            $sacas_atual=$peso_atual/60;


            $valortemp=($valor_saca/60)*$peso_atual;

            $infovolumes=$infovolumes+$sacas_atual;
            $infovalor=$infovalor+$valortemp;

            echo "<tr class='{$cordalinha}'>";
            echo "<td><small>".fncgetprodutos($lote['tipo_cafe'])['abrev']."</small></td>";
            echo "<td><small>".$cata."</small></td>";
            echo "<td><small>".strtoupper(fncgetpessoa($entrada['fornecedor'])['nome'])."</small></td>";
            echo "<td style='white-space: nowrap;'><small>".$romaneio_tipo.$entrada['romaneio']."-".$letra." ".dataRetiraHora($lote['data_ts'])."</small></td>";
            echo "<td><small>".number_format($sacas_atual, 2, ',', '.')."v,   ".$peso_atual."kg </small></td>";
            echo "<td class='text-center'>R$ ".$valor_saca."</td>";

            echo "<td>".$lote["bo"]."</td>";
            echo "</tr>";
        }
        $infovalor1=@($infovalor/$infovolumes);
        $infovalor1=number_format($infovalor1, 2, ',', '.');
        $infovalor2=$infovalor;
        $infovalor2=number_format($infovalor2, 2, ',', '.');
        ?>
        </tbody>
    </table>


    <table class="table table-bordered table-hover table-sm">
        <tbody>
        <tr>
            <td>QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo $infovolumes; ?></strong></td>
            <td>VALOR MÉDIA POR VOLUMES: R$ <strong><?php echo $infovalor1; ?></strong></td>
            <td>VALOR TOTAL APROXIMADO: R$ <strong><?php echo $infovalor2; ?></strong></td>
        </tr>
        </tbody>
    </table>


    <?php
    $sql = "SELECT sum(`peso_atual`) FROM ztst_entradas_lotes WHERE status=1 and peso_atual<>0 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $estoquetudo = $consulta->fetch();
    ?>
    <table class="table table-sm text-left table-responsive table-striped table-hover">
        <tr class="">
            <td>CAFÉ EM ESTOQUE</td>
            <td><?php
                $sacas=$estoquetudo[0]/60;
                $sacas=number_format($sacas, 2, '.', ',');
                $peso=$estoquetudo[0]." Kg";
                echo $sacas." saca(s) ou ".$peso;
                ?></td>
        </tr>

        <?php
        foreach (fncprodutoslist() as $produtos){
            $sql = "SELECT sum(`peso_atual`) FROM ztst_entradas_lotes WHERE status=1 and peso_atual<>0 and tipo_cafe=? ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$produtos['id']);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $estoqueportipo = $consulta->fetch();

            $sacas=$estoqueportipo[0]/60;
            $sacas=number_format($sacas, 2, '.', ',');
            $peso=$estoqueportipo[0]." Kg";

            if ($estoqueportipo[0]==0 or $estoqueportipo[0]==null or $estoqueportipo[0]=="" or !is_numeric($estoqueportipo[0])){
                $exib= " d-none";
            }else{
                $exib= " ";
            }

            echo "<tr class='{$exib}'>";
            echo "<td>";
            echo $produtos['nome'];
            echo "</td>";
            echo "<td>";
            echo $sacas." saca(s) ou ".$peso;
            echo "</td>";
            echo "</tr>";

        }
        ?>

    </table>

</div>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>