<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
    $a="vendasave";
    $venda=fncgetvenda($_GET['id_v']);

}else{
    $a="vendanew";
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

    $(document).ready(function () {
        $('#serchcliente input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultcliente");
            if (inputValf.length) {
                $.get("includes/cliente.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultcliente p", function () {
            var cliente = this.id;
            $.when(
                // $(this).parents("#serchcliente").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchcliente").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultcliente").empty()

                $(this).parents("#serchcliente").find('input[type="text"]').val($(this).text()),
                $('#cliente').val(cliente),
                $(this).parent(".resultcliente").empty()

            ).then(
                function() {
                    // roda depois de acao1 e acao2 terminarem
                    // setTimeout(function() {
                    //     //do something special
                    //     $( "#gogo" ).click();
                    // }, 500);
                });
        });
    });

    $(document).ready(function () {
        $('#serchtransportadora input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resulttransportadora");
            if (inputValf.length) {
                $.get("includes/transportadora.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resulttransportadora p", function () {
            var transportadora = this.id;
            $.when(
                // $(this).parents("#serchtransportadora").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchtransportadora").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resulttransportadora").empty()

                $(this).parents("#serchtransportadora").find('input[type="text"]').val($(this).text()),
                $('#transportadora').val(transportadora),
                $(this).parent(".resulttransportadora").empty()

            ).then(
                function() {
                    // roda depois de acao1 e acao2 terminarem
                    // setTimeout(function() {
                    //     //do something special
                    //     $( "#gogo" ).click();
                    // }, 500);
                });
        });
    });

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vv&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de vendas</h3>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $venda['id']; ?>"/>
                <label for="prefixo">PREFIXO</label>
                <input autocomplete="off" id="prefixo" type="text" class="form-control" name="prefixo" value="<?php echo $venda['prefixo']; ?>" placeholder="Preencha com o prefixo do contrato" maxlength="5" />
            </div>

            <div class="col-md-4">
                <label for="contrato">NÚMERO DE CONTRATO</label>
                <input autocomplete="off" id="contrato" type="text" class="form-control" name="contrato" value="<?php echo $venda['contrato']; ?>" placeholder="Preencha com o número de contrato"/>
            </div>

            <div class="col-md-4">
                <label for="descricao">DESCRIÇÃO DO CONTRATO</label>
                <input autocomplete="off" id="descricao" type="text" class="form-control" name="descricao" value="<?php echo $venda['descricao']; ?>" placeholder="Preencha com a descrição"/>
            </div>


        </div>

        <div class="row">
            <div class="col-md-6" id="serchcliente">
                <label for="cliente">CLIENTE</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="r_corretor">
                            <a href="index.php?pg=Vpessoa_lista" target="_blank" class="fa fas fa-plus text-success "></a>
                        </span>
                    </div>
                    <?php
                    if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
                        $v_cliente_id=$venda['cliente'];
                        $v_cliente=fncgetpessoa($venda['cliente'])['nome'];

                    }else{
                        $v_cliente_id="";
                        $v_cliente="";
                    }
                    $c_cliente = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="c<?php echo $c_cliente;?>" id="c<?php echo $c_cliente;?>" value="<?php echo $v_cliente; ?>" placeholder="Click no resultado ao aparecer" required/>
                    <input id="cliente" autocomplete="false" type="hidden" class="form-control" name="cliente" value="<?php echo $v_cliente_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_cliente">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultcliente"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_fornecedor").click(function(ev){
                            document.getElementById('cliente').value='';
                            document.getElementById('c<?php echo $c_cliente;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-6" id="serchtransportadora">
                <label for="transportadora">TRANSPORTADORA</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="r_corretor">
                            <a href="index.php?pg=Vpessoa_lista" target="_blank" class="fa fas fa-plus text-success "></a>
                        </span>
                    </div>
                    <?php
                    if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
                        $v_transportadora_id=$venda['transportadora'];
                        $v_transportadora=fncgetpessoa($venda['transportadora'])['nome'];

                    }else{
                        $v_transportadora_id="";
                        $v_transportadora="";
                    }
                    $c_transportadora = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="c<?php echo $c_transportadora;?>" id="c<?php echo $c_transportadora;?>" value="<?php echo $v_transportadora; ?>" placeholder="Click no resultado ao aparecer" required/>
                    <input id="transportadora" autocomplete="false" type="hidden" class="form-control" name="transportadora" value="<?php echo $v_transportadora_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_transportadora">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resulttransportadora"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_fornecedor").click(function(ev){
                            document.getElementById('transportadora').value='';
                            document.getElementById('c<?php echo $c_transportadora;?>').value='';
                        });
                    });
                </script>
            </div>


            <div class="col-md-12">
                <label for="nota_fiscal">NOTA FISCAL</label>
                <input autocomplete="off" id="nota_fiscal" type="text" class="form-control" name="nota_fiscal" value="<?php echo $venda['nota_fiscal']; ?>" placeholder="Preencha com as notas fiscais"/>
            </div>

            <div class="col-md-12">
                <label for="nota_fiscal">RETORNO DE ESTOQUE</label>
                <input autocomplete="off" id="retorno_de_estoque" type="text" class="form-control" name="retorno_de_estoque" value="<?php echo $venda['retorno_de_estoque']; ?>" placeholder="preencha com a informação de retorno de estoque se houver"/>
            </div>

            <div class="col-md-6">
                <label for="status">STATUS:</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="r_corretor">
                            <i class="fa fas fa-unlock-alt text-success"></i>
                        </span>
                    </div>
                    <select name="status" id="status" class="form-control" required>
                        <option selected="" value="<?php
                        if(is_null($venda['status'])){
                            echo 1;
                        }
                        if($venda['status']=="0"){
                            echo 0;
                        }
                        if($venda['status']=="1"){
                            echo 1;
                        }
                        ?>">
                            <?php
                            if(is_null($venda['status'])){echo"ATIVO";}
                            if($venda['status']=="0"){echo"FECHADO";}
                            if($venda['status']=="1"){echo"ATIVO";} ?>
                        </option>
                        <option value="0">FECHADO</option>
                        <option value="1">ATIVO</option>
                    </select>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" name="gogo" id="gogo" class="btn btn-block btn-success mt-4">
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>

    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>