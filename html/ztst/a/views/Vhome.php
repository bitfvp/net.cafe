<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}'>";
include_once("includes/topo.php");

$sql = "SELECT sum(`peso_atual`) FROM ztst_entradas_lotes WHERE status=1 and peso_atual<>0 ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$estoquetudo = $consulta->fetch();
?>

<main class="container-fluid"><!--todo conteudo-->


    <div class="row">
        <div class="col-md-4 bg-light text-center">
            <div class="card">

                <div class="card-body">
                    <table class="table table-sm text-left table-striped table-hover">
                        <tr class="" id="destacar">
                            <td>CAFÉ EM ESTOQUE</td>
                            <td style="white-space: nowrap;"><?php
                                $sacas=$estoquetudo[0]/60;
                                $peso=$estoquetudo[0];
                                echo number_format($sacas,2)."v<br>".number_format($peso,2)."Kg";
                                ?>
                            </td>
                        </tr>

                        <style type="text/css">
                            #destacar {
                                background: linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab);
                                background-size: 400% 400%;
                                -webkit-animation: gradient 15s ease infinite;
                                animation: gradient 15s ease infinite;
                            }

                            @-webkit-keyframes gradient {
                                0% {
                                    background-position: 0% 50%;
                                }
                                50% {
                                    background-position: 100% 50%;
                                }
                                100% {
                                    background-position: 0% 50%;
                                }
                            }

                            @keyframes gradient {
                                0% {
                                    background-position: 0% 50%;
                                }
                                50% {
                                    background-position: 100% 50%;
                                }
                                100% {
                                    background-position: 0% 50%;
                                }
                            }
                        </style>



                        <?php
                        foreach (fncprodutoslist() as $produtos){
                            $sql = "SELECT sum(`peso_atual`) FROM ztst_entradas_lotes WHERE status=1 and peso_atual<>0 and tipo_cafe=? ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1,$produtos['id']);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $estoqueportipo = $consulta->fetch();

                            $sacas=$estoqueportipo[0]/60;
                            $peso=$estoqueportipo[0];

                            if ($estoqueportipo[0]==0 or $estoqueportipo[0]==null or $estoqueportipo[0]=="" or !is_numeric($estoqueportipo[0])){
                                $exib= " d-none";
                            }else{
                                $exib= " ";
                            }

                            echo "<tr class='{$exib}'>";
                                echo "<td>";
                                    echo $produtos['nome'];
                                echo "</td>";
                            echo "<td style='white-space: nowrap;'>";
                            echo number_format($sacas,2)."v<br>".number_format($peso,2)."Kg";
                            echo "</td>";
                            echo "</tr>";

                        }
                        ?>

                    </table>
                </div>
            </div>
        </div>
    </div>




</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>