<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

//contagem de tempo de processamento,
// fim da contagem e apresentação da informacão
//esta contida no final de ~~includes/footer.php
list($usec, $sec) = explode(' ', microtime());
$script_start = (float) $sec + (float) $usec;

////inclui o documento com os ponteiros dizendo onde estão as classes, contollers e includes
include_once("includes/header.php");

////da loop nas Supervariaveis e cria variaveis com mesmo nome e valores correspondendes
foreach($_REQUEST as $___opt => $___val){
    $$___opt = $___val;
}
//se nao estiver setada ou estiver vazia carregara o view padrão
if(!isset($pg) || empty($pg)){
    //incluindo view padrao
    include_once("views/Vhome.php");
}else{
    //se chegou ate aqui vai carregar na variavel xxfile o caminho do view
    $file="views/".$pg.".php";
    //ira testar se éxiste esse arquivo nesse caminho
    if(file_exists($file)){
        //se existe carregara o view
        include_once($file);
    }else{
        //senao carregara uma pagina de erro
        include_once('views/404.php');
    }
}