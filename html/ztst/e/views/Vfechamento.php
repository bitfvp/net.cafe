<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vf_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $fechamento=fncgetfechamento($_GET['id']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vfechamento_lista");
    exit();
}

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Fechamento
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <header>
                                NEGOCIAÇÃO DO TIPO:
                                <strong class="text-info">
                                    <?php
                                    if ($fechamento['tipo_fechamento']==1){
                                        echo "COMPRA";
                                    }
                                    if ($fechamento['tipo_fechamento']==2){
                                        echo "VENDA";
                                    }
                                    echo " -- ";
                                    echo utf8_encode(strftime('%Y', strtotime("{$fechamento['data_ts']}")))."-".$fechamento['id']."<br>";
                                    if (strlen($fechamento['ordem_compra'])>0){
                                        echo "<p class='text-dark mb-1'>ORDEM DE COMPRA:".strtoupper($fechamento['ordem_compra'])."</p>";
                                    }
                                    echo datahoraBanco2data($fechamento['data_ts']);
                                    ?>
                                </strong>
                            </header>
                        </div>
                    </div>

                    <h6>
                        VENDEDOR:
                        <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['vendedor'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                        COMPRADOR:
                        <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['comprador'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                        LOCAL DE DESCARGA:
                        <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['descarga'])['nome']); ?>&nbsp;&nbsp;</strong><br>
<!--                        CORRETAGEM DE COMPRA:-->
<!--                        <strong class="text-info text-uppercase">--><?php //if ($fechamento['corretagem_c']>0){echo $fechamento['corretagem_c']."%";}else{echo 0;} ; ?><!--&nbsp;&nbsp;</strong><br>-->
<!--                        CORRETAGEM DE VENDA:-->
<!--                        <strong class="text-info text-uppercase">--><?php //if ($fechamento['corretagem_v']>0){echo $fechamento['corretagem_v']."%";}else{echo 0;} ; ?><!--&nbsp;&nbsp;</strong><br>-->

                        <hr>

                        <?php include_once("includes/fechamento_form.php");?>


                        DESCRIÇÃO:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['descricao']); ?>&nbsp;&nbsp;</strong><br>
                        CONDIÇÃO DE PAGAMENTO:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['condicao_pag']); ?>&nbsp;&nbsp;</strong><br>
                        FORMA DE PAGAMENTO:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['forma_pag']); ?>&nbsp;&nbsp;</strong><br>
                        CONDIÇÃO DE ENTREGA:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['condicao_entrega']); ?>&nbsp;&nbsp;</strong><br>
                        PRAZO DE ENTREGA:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['prazo_entrega']); ?>&nbsp;&nbsp;</strong><br>

                        CORRETOR:
                        <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetcorretor($fechamento['corretor'])['corretor']); ?>&nbsp;&nbsp;</strong><br>
                        OBSERVAÇÃO:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['observacao']); ?>&nbsp;&nbsp;</strong><br>
                    </h6>

                    <div class="row">
                    <div class="col-md-6">
                        <a href="index.php?pg=Vfechamento_editar&id=<?php echo $_GET['id'] ?>" title="Editar entrada" class="btn btn-primary btn-block">EDITAR FECHAMENTO</a>
                    </div>

                        <div class="col-md-6">
                            <a href="index.php?pg=Vfechamento_print1&id=<?php echo $_GET['id']; ?>" target="_blank" class="btn btn-info btn-block">Impressão de comprovante</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

<?php
    try{
        $sql="SELECT * FROM ";
        $sql.="ztst_caixa_lancamentos ";
        $sql.="WHERE fechamento_id=:id and status=1";
        global $pdo;
        $consulta=$pdo->prepare($sql);
        $consulta->bindValue(":id", $_GET['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $lancamentos=$consulta->fetchAll();
    ?>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Lançamentos de caixa
                </div>
                <div class="card-body">
                    <a href="index.php?pg=Vfechamento2&id=<?php echo $_GET['id'];?>" class="btn btn-info btn-block">IR PARA LANÇAMENTOS NO CAIXA <i class="fas fa-arrow-right"></i></a>

                        <?php
                        $valor_acumulado=0;
                        foreach ($lancamentos as $dado){
                            $valor_acumulado+=$dado['valor'];
                        }
                        ?>

                    <hr>
                    <?php if ($valor_acumulado<$sum){$tempcor="danger";}else{$tempcor="success";}?>
                    <h6 class="form-cadastro-heading text-<?php echo $tempcor;?>">VALOR PROJETADO NO CAIXA: <BR>
                        R$ <?php echo number_format($valor_acumulado,2, ',', ' ');?>   /   R$ <?php echo number_format($sum,2, ',', ' ');?></h6>

                </div>
            </div>

        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>