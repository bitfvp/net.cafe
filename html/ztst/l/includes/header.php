<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

//reports
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/net.cafe/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/net.cafe/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();
/*
*   inclui e inicia a classe de configuração de ambiente que é herdada de uma classe abaixo da raiz em models/Env.class.php
*   $env e seus atributos sera usada por todo o sistema
*/
include_once("models/ConfigMod.class.php");
$env=new ConfigMod();

/*
*   inclui e inicia a classe de configuração de drive pdo/mysql
*   $pdo e seus atributos sera usada por todas as querys
*/
include_once("{$env->env_root}models/Db.class.php");////conecção com o db
$pdo = Db::conn();


use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Device;
use Sinergi\BrowserDetector\Language;
$browser = new Browser();
$os = new Os();
$device = new Device();
$language = new Language();

//classe para debugar e salvar sqls ao banco de dados
include_once("{$env->env_root}models/LogQuery.class.php");//classe de log de query
$LQ = new LogQuery();


//inclui um controle contendo a funcão que apaga a sessao e desloga
//será chamada da seguinte maneira
//killSession();
include_once("{$env->env_root}controllers/Ks.php");//se ha uma acao

//inclui um controle que ativa uma acao
include_once("{$env->env_root}controllers/action.php");//se ha uma acao

//funcoes diversas
include_once ("{$env->env_root}includes/funcoes.php");//funcoes

////troca tema
include_once("{$env->env_root}controllers/trocatema.php");
////logout
include_once("{$env->env_root}controllers/logout.php");

//valida o token e gera array
include_once("{$env->env_root}controllers/validaToken.php");

//valida se há manutenção
include_once("{$env->env_root}controllers/validaManutencao.php");

//chat msg
include_once("{$env->env_root}models/ztst/Chat_Msg.class.php");
include_once("{$env->env_root}controllers/ztst/chat_msg.php");

/* inicio do Bloco dedidado*/
if (isset($_SESSION['logado']) and $_SESSION['logado']=="1"){

    include_once("{$env->env_root}controllers/usuario_lista.php");


    //pessoa
    include_once("models/Pessoa.class.php");
    include_once("controllers/pessoa_carregar.php");
    include_once("controllers/pessoa_edicao.php");
    include_once("controllers/pessoa_novo.php");
    include_once("controllers/pessoa_apagar.php");
    include_once ("{$env->env_root}includes/ztst/rank_pessoa.php");

    //veiculos
    include_once("models/Veiculo.class.php");
    include_once("controllers/veiculo_load.php");
    include_once("controllers/veiculo_insert.php");
    include_once("controllers/veiculo_update.php");
    include_once("controllers/veiculo_delete.php");

    //veiculos itens
    include_once("models/Veiculo_item.class.php");
    include_once("controllers/veiculo_item_load.php");
    include_once("controllers/veiculo_item_insert.php");
    include_once("controllers/veiculo_item_update.php");
    include_once("controllers/veiculo_item_delete.php");

    //veiculos at
    include_once("models/Veiculo_at.class.php");
    include_once("controllers/veiculo_at_load.php");
    include_once("controllers/veiculo_at_insert.php");
    include_once("controllers/veiculo_at_update.php");
    include_once("controllers/veiculo_at_delete.php");


    //viagens
    include_once("models/Viagem.class.php");
    include_once("controllers/viagem_load.php");
    include_once("controllers/viagem_insert.php");
    include_once("controllers/viagem_update.php");
    include_once("controllers/viagem_delete.php");

    //viagem at
    include_once("models/Viagem_at.class.php");
    include_once("controllers/viagem_at_load.php");
    include_once("controllers/viagem_at_insert.php");
    include_once("controllers/viagem_at_update.php");
    include_once("controllers/viagem_at_delete.php");

    //abastecimento
    include_once("models/Abastecimento.class.php");
    include_once("controllers/abastecimento_load.php");
    include_once("controllers/abastecimento_insert.php");
    include_once("controllers/abastecimento_update.php");
    include_once("controllers/abastecimento_delete.php");


}
/* fim do bloco dedicado*/

//metodo de checar usuario
include_once("{$env->env_root}controllers/confirmUser.php");