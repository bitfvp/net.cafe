<?php
class Viagem_at{
    public function fncviagem_atinsert($id_viagem, $id_veiculo, $tipo, $km, $valor, $descricao, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO ztst_log_viagens_at ";
            $sql.="(id, id_viagem, tipo, km, valor, descricao, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :id_viagem, :tipo, :km, :valor, :descricao, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":id_viagem", $id_viagem);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":km", $km);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            try {
                $sql="UPDATE ztst_log_veiculos ";
                $sql.="SET ";
                $sql .= "
                km=:km
                WHERE id=:id and km<=:km_troca";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":km", $km);
                $atualiza->bindValue(":id", $id_veiculo);
                $atualiza->bindValue(":km_troca", $km);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }


            header("Location: index.php?pg=Vvi&id={$id_viagem}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncviagem_atupdate($id, $id_viagem, $tipo, $km, $valor, $descricao ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ztst_log_viagens_at ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ztst_log_viagens_at ";
                $sql.="SET ";
                $sql .= "id_viagem=:id_viagem,
tipo=:tipo,
km=:km,
valor=:valor,
descricao=:descricao
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":id_viagem", $id_viagem);
                $atualiza->bindValue(":tipo", $tipo);
                $atualiza->bindValue(":km", $km);
                $atualiza->bindValue(":valor", $valor);
                $atualiza->bindValue(":descricao", $descricao);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncviagem_atdelete($tabela_id,$usuario_off ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ztst_log_viagens_at ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ztst_log_viagens_at ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe