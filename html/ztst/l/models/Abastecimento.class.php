<?php
class Abastecimento{
    public function fncabastecimentoinsert($id_veiculo, $id_motorista, $data, $quantidade, $valor, $km, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO ztst_log_veiculos_abastecimentos ";
            $sql.="(id, id_veiculo, id_motorista, data, quantidade, valor, km, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :id_veiculo, :id_motorista, :data, :quantidade, :valor, :km, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":id_veiculo", $id_veiculo);
            $insere->bindValue(":id_motorista", $id_motorista);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":quantidade", $quantidade);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":km", $km);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];


            try {
                $sql="UPDATE ztst_log_veiculos ";
                $sql.="SET ";
                $sql .= "
                km=:km
                WHERE id=:id and km<=:km_troca";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":km", $km);
                $atualiza->bindValue(":id", $id_veiculo);
                $atualiza->bindValue(":km_troca", $km);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }



            header("Location: index.php?pg=Vve_a&id_veiculo={$id_veiculo}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncabastecimentoupdate($id, $id_veiculo, $id_motorista, $data, $quantidade, $valor, $km ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ztst_log_veiculos_abastecimentos ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ztst_log_veiculos_abastecimentos ";
                $sql.="SET ";
                $sql .= "id_veiculo=:id_veiculo,
id_motorista=:id_motorista,
data=:data,
quantidade=:quantidade,
valor=:valor,
km=:km
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":id_veiculo", $id_veiculo);
                $atualiza->bindValue(":id_motorista", $id_motorista);
                $atualiza->bindValue(":data", $data);
                $atualiza->bindValue(":quantidade", $quantidade);
                $atualiza->bindValue(":valor", $valor);
                $atualiza->bindValue(":km", $km);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vve_a&id_veiculo={$id_veiculo}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncabastecimentodelete($tabela_id,$usuario_off ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ztst_log_veiculos_abastecimentos ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ztst_log_veiculos_abastecimentos ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe