<?php
class Veiculo_item{
    public function fncveiculo_iteminsert($id_veiculo, $tipo, $linha, $coluna, $km_troca, $km_futuro, $data_revisao, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO ztst_log_veiculos_itens ";
            $sql.="(id, id_veiculo, tipo, linha, coluna, km_troca, km_futuro, data_revisao, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :id_veiculo, :tipo, :linha, :coluna, :km_troca, :km_futuro, :data_revisao, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":id_veiculo", $id_veiculo);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":linha", $linha);
            $insere->bindValue(":coluna", $coluna);
            $insere->bindValue(":km_troca", $km_troca);
            $insere->bindValue(":km_futuro", $km_futuro);
            $insere->bindValue(":data_revisao", $data_revisao);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php?pg=Vve&id={$id_veiculo}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncveiculo_itemupdate($id, $id_veiculo, $tipo, $linha, $coluna, $km_troca, $km_futuro, $data_revisao ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ztst_log_veiculos_itens ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ztst_log_veiculos_itens ";
                $sql.="SET ";
                $sql .= "id_veiculo=:id_veiculo,
tipo=:tipo,
linha=:linha,
coluna=:coluna,
km_troca=:km_troca,
km_futuro=:km_futuro,
data_revisao=:data_revisao
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":id_veiculo", $id_veiculo);
                $atualiza->bindValue(":tipo", $tipo);
                $atualiza->bindValue(":linha", $linha);
                $atualiza->bindValue(":coluna", $coluna);
                $atualiza->bindValue(":km_troca", $km_troca);
                $atualiza->bindValue(":km_futuro", $km_futuro);
                $atualiza->bindValue(":data_revisao", $data_revisao);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vve&id={$id_veiculo}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncveiculo_itemdelete($tabela_id,$usuario_off ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ztst_log_veiculos_itens ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ztst_log_veiculos_itens ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe