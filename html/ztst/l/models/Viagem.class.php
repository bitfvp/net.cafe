<?php
class Viagem{
    public function fncviageminsert($contratante, $caminhao, $motorista, $carga, $saida, $destino, $valor, $descricao, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO ztst_log_viagens ";
            $sql.="(id, contratante, caminhao, motorista, carga, saida, destino, valor, descricao, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :contratante, :caminhao, :motorista, :carga, :saida, :destino, :valor, :descricao, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":contratante", $contratante);
            $insere->bindValue(":caminhao", $caminhao);
            $insere->bindValue(":motorista", $motorista);
            $insere->bindValue(":carga", $carga);
            $insere->bindValue(":saida", $saida);
            $insere->bindValue(":destino", $destino);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM ";
            $sql.="ztst_log_viagens";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $maid=$mid[0];
            header("Location: index.php?pg=Vvi&id={$maid}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncviagemupdate($id, $contratante, $caminhao, $motorista, $carga, $saida, $destino, $valor, $descricao ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ztst_log_viagens ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ztst_log_viagens ";
                $sql.="SET ";
                $sql .= "contratante=:contratante,
caminhao=:caminhao,
motorista=:motorista,
carga=:carga,
saida=:saida,
destino=:destino,
valor=:valor,
descricao=:descricao
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":contratante", $contratante);
                $atualiza->bindValue(":caminhao", $caminhao);
                $atualiza->bindValue(":motorista", $motorista);
                $atualiza->bindValue(":carga", $carga);
                $atualiza->bindValue(":saida", $saida);
                $atualiza->bindValue(":destino", $destino);
                $atualiza->bindValue(":valor", $valor);
                $atualiza->bindValue(":descricao", $descricao);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vvi&id={$id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncviagemdelete($tabela_id,$usuario_off ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ztst_log_viagens ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ztst_log_viagens ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe