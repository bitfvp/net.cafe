<?php
//gerado pelo geracode
function fncviagem_atlist(){
    $sql = "SELECT * FROM ztst_log_viagens_at ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $viagem_atlista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $viagem_atlista;
}

function fncgetviagem_at($id){
    $sql = "SELECT * FROM ztst_log_viagens_at WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getztst_log_viagens_at = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getztst_log_viagens_at;
}
?>