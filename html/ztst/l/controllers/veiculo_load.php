<?php
//gerado pelo geracode
function fncveiculolist(){
    $sql = "SELECT * FROM ztst_log_veiculos ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $veiculolista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $veiculolista;
}

function fncgetveiculo($id){
    $sql = "SELECT * FROM ztst_log_veiculos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getztst_log_veiculos = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getztst_log_veiculos;
}
?>