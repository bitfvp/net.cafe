<?php
function fncpessoalist(){
    $sql = "SELECT * FROM ztst_log_pessoas ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pessoalista;
}

function fncgetpessoa($id){
    $sql = "SELECT * FROM ztst_log_pessoas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getpessoa = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getpessoa;
}

function fncpessoaresponsavellist(){
    $sql = "SELECT * FROM ztst_log_pessoas where p_responsavel=1 ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pessoalista;
}

function fncpessoamotoristalist(){
    $sql = "SELECT * FROM ztst_log_pessoas where p_motorista=1 ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pessoalista;
}
?>