<?php
//gerado pelo geracode
function fncveiculo_itemlist(){
    $sql = "SELECT * FROM ztst_log_veiculos_itens ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $veiculo_itemlista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $veiculo_itemlista;
}

function fncgetveiculo_item($id){
    $sql = "SELECT * FROM ztst_log_veiculos_itens WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getztst_log_veiculos_itens = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getztst_log_veiculos_itens;
}
?>