<?php
//gerado pelo geracode
function fncabastecimentolist(){
    $sql = "SELECT * FROM ztst_log_veiculos_abastecimentos ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $abastecimentolista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $abastecimentolista;
}

function fncgetabastecimento($id){
    $sql = "SELECT * FROM ztst_log_veiculos_abastecimentos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getztst_log_veiculos_abastecimentos = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getztst_log_veiculos_abastecimentos;
}
?>