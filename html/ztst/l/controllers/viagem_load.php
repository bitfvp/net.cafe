<?php
//gerado pelo geracode
function fncviagemlist(){
    $sql = "SELECT * FROM ztst_log_viagens ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $viagemlista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $viagemlista;
}

function fncgetviagem($id){
    $sql = "SELECT * FROM ztst_log_viagens WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getztst_log_viagens = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getztst_log_viagens;
}
?>