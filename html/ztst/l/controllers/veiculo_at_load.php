<?php
//gerado pelo geracode
function fncveiculo_atlist(){
    $sql = "SELECT * FROM ztst_log_veiculos_at ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $veiculo_atlista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $veiculo_atlista;
}

function fncgetveiculo_at($id){
    $sql = "SELECT * FROM ztst_log_veiculos_at WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getztst_log_veiculos_at = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getztst_log_veiculos_at;
}
?>