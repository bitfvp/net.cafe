<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar veiculo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="veiculoupdate";
    $veiculo=fncgetveiculo($_GET['id']);


}else{
    $a="veiculoinsert";

}
?>
<div class="container">
    <form class="form-signin" action="<?php echo "index.php?pg=Vve_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de veiculo</h3>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $veiculo['id']; ?>"/>
                <label for="modelo">MODELO:</label>
                <input autocomplete="off" autofocus id="modelo" placeholder="Marca e modelo" type="text" class="form-control" name="modelo" value="<?php echo $veiculo['modelo']; ?>" required/>
            </div>
            <div class="col-md-4">
                <label for="placa">PLACA:</label>
                <input autocomplete="off" id="placa" type="text" class="form-control" name="placa" value="<?php echo $veiculo['placa']; ?>" required/>
            </div>
            <div class="col-md-8">
                <label for="outros">OUTROS:</label>
                <input autocomplete="off" id="outros" type="text" class="form-control" name="outros" value="<?php echo $veiculo['outros']; ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="km">KILOMETRAGEM:</label>
                <input id="km" type="number" autocomplete="off" class="form-control" name="km" value="<?php echo $veiculo['km']; ?>" min="0" required/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>