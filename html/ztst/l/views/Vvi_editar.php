<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vf_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="viagemupdate";
    $viagem=fncgetviagem($_GET['id']);
}else{
    $a="viageminsert";
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

    $(document).ready(function () {
        $('#serchcontratante input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultcontratante");
            if (inputValf.length) {
                $.get("includes/cliente.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultcontratante p", function () {
            var contratante = this.id;
            $.when(
                // $(this).parents("#serchcontratante").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchcontratante").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultcontratante").empty()

                $(this).parents("#serchcontratante").find('input[type="text"]').val($(this).text()),
                $('#contratante').val(contratante),
                $(this).parent(".resultcontratante").empty()

            ).then(
                // function() {
                //     // roda depois de acao1 e acao2 terminarem
                //     setTimeout(function() {
                //         //do something special
                //         $( "#gogo" ).click();
                //     }, 500);
                // }
                );
        });
    });


</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vvi_lista&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Viagem</h3>
        <hr>
        <div class="row">

            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $viagem['id']; ?>"/>


            <div class="col-md-12" id="serchcontratante">
                <label for="contratante">CONTRATANTE:</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_contratante_id=$viagem['contratante'];
                        $v_contratante=fncgetpessoa($viagem['contratante'])['nome'];

                    }else{
                        $v_contratante_id="";
                        $v_contratante="";
                    }
                    $c_contratante = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input  autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="c<?php echo $c_contratante;?>" id="c<?php echo $c_contratante;?>" value="<?php echo $v_contratante; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input id="contratante" autocomplete="false" type="hidden" class="form-control" name="contratante" value="<?php echo $v_contratante_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_contratante">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultcontratante"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_contratante").click(function(ev){
                            document.getElementById('contratante').value='';
                            document.getElementById('c<?php echo $c_contratante;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-6">
                <label for="caminhao">CAMINHÃO:</label>
                <select name="caminhao" id="caminhao" class="form-control input-sm" data-live-search="true" required>
                    <?php
                    $getcaminhao=fncgetveiculo($viagem['caminhao']);
                    ?>
                    <option selected="" data-tokens="<?php echo $getcaminhao['modelo'];?>" value="<?php echo $viagem['caminhao']; ?>">
                        <?php echo $getcaminhao['modelo'];?>
                    </option>
                    <?php
                    foreach (fncveiculolist() as $item) {
                        ?>
                        <option data-tokens="<?php echo $item['modelo'];?>" value="<?php echo $item['id'];?>">
                            <?php echo strtoupper($item['modelo']); ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="col-md-6">
                <label for="motorista">MOTORISTA:</label>
                <select name="motorista" id="motorista" class="form-control input-sm" data-live-search="true" required>
                    <?php
                    $getmotorista=fncgetpessoa($viagem['motorista']);
                    ?>
                    <option selected="" data-tokens="<?php echo $getmotorista['nome'];?>" value="<?php echo $viagem['motorista']; ?>">
                        <?php echo $getmotorista['nome'];?>
                    </option>
                    <?php
                    foreach (fncpessoamotoristalist() as $item) {
                        ?>
                        <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                            <?php echo strtoupper($item['nome']); ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="col-md-12">
                <label for="carga">CARGA:</label>
                <input autocomplete="off" id="carga" type="text" class="form-control" name="carga" value="<?php echo $viagem['carga']; ?>"/>
            </div>

            <div class="col-md-6">
                <label for="saida">SAÍDA DE:</label>
                <input autocomplete="off" id="saida" type="text" class="form-control" name="saida" value="<?php echo $viagem['saida']; ?>"/>
            </div>

            <div class="col-md-6">
                <label for="destino">DESTINO:</label>
                <input autocomplete="off" id="destino" type="text" class="form-control" name="destino" value="<?php echo $viagem['destino']; ?>"/>
            </div>

            <div class="col-md-4">
                <label for="valor">VALOR:</label>
                <div class="input-group">
                    <input autocomplete="off" id="valor" placeholder="" type="text" class="form-control" name="valor" value="<?php echo $viagem['valor']; ?>" required />
                    <div class="input-group-append">
                        <span class="input-group-text">,00 R$</span>
                    </div>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#valor').mask('000000', {reverse: true});
                    });
                </script>
            </div>

            <div class="col-md-12">
                <label for="descricao">DESCRIÇÃO:</label>
                <textarea id="descricao" onkeyup="limite_textarea(this.value,1000,descricao,'cont')" maxlength="1000" class="form-control" rows="2" name="descricao" required><?php echo $viagem['descricao']; ?></textarea>
                <span id="cont">1000</span>/1000
            </div>




            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" name="gogo" id="gogo" class="btn btn-block btn-success mt-4">
            </div>

            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>


        </div>

    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>