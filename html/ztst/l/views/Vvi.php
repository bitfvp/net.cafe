<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="viagem-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $viagem=fncgetviagem($_GET['id']);
    $contratante = fncgetpessoa($viagem['contratante']);
    $motorista = fncgetpessoa($viagem['motorista']);
    $veiculo = fncgetveiculo($viagem["caminhao"]) ;
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}

?>
<main class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados da viagem
                </div>
                <div class="card-body text-uppercase">
                    <blockquote class="blockquote blockquote-info">
                        <header>CONTRATANTE: <strong class="text-info"><?php echo $contratante['nome']; ?>&nbsp;&nbsp;</strong></header>
                        <h5>
                            DATA: <strong class="text-info"><?php echo dataRetiraHora($viagem["data_ts"]);; ?></strong>
                        </h5>
                        <h5>
                            CAMINHÃO: <strong class="text-info"><?php echo $veiculo['modelo']; ?></strong>
                        </h5>
                        <h5>
                            MOTORISTA: <strong class="text-info"><?php echo $motorista['nome']; ?></strong>
                        </h5>
                        <h5>
                            CARGA: <strong class="text-info"><?php echo $viagem['carga']; ?></strong>
                            VALOR: <strong class="text-info">R$<?php echo number_format($viagem['valor'],0,',','.'); ?></strong>
                        </h5>
                        <h5>
                            SAÍDA DE: <strong class="text-info"><?php echo $viagem['saida']; ?></strong>
                            DESTINO: <strong class="text-info"><?php echo $viagem['destino']; ?></strong>
                        </h5>
                        <h5>
                            DESCRIÇÃO: <strong class="text-info"><?php echo $viagem['descricao']; ?></strong>
                        </h5>
                    </blockquote>
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vvi_editar&id=<?php echo $_GET['id']; ?>" title="Editar entrada" class="btn btn-sm btn-primary fas fa-pen text-dark">
                            <br>EDITAR VIAGEM
                        </a>
                        <a href="index.php?pg=Vvi_print&id=<?php echo $_GET['id']; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark fas fa-print">
                            <br>COMPROVANTE DE GASTOS
                        </a>

                    </div>
                </div>
                <!-- fim da col md 4 -->
            </div>
        </div>
    </div>





    <div class="card mt-2">
        <div class="card-body">
            <form class="form-signin" action="<?php echo "index.php?pg=Vvi&aca=viagem_atinsert"; ?>" method="post" id="formx">
                <h3 class="form-cadastro-heading">Cadastrar ocorrências da viagem</h3>
                <hr>
                <div class="row">
                    <input id="id_viagem" type="hidden" class="txt bradius" name="id_viagem" value="<?php echo $_GET['id']; ?>"/>
                    <input id="id_veiculo" type="hidden" class="txt bradius" name="id_veiculo" value="<?php echo $viagem["caminhao"]; ?>"/>

                    <div class="col-md-4">
                        <label for="tipo">TIPO:</label>
                        <select name="tipo" id="tipo" class="form-control" required>
                            <option selected="" value="">
                                Selecione...
                            </option>
                            <option value="1">Carregamento</option>
                            <option value="2">Descarregamento</option>
                            <option value="3">Gastos</option>
                            <option value="4">Deslocamento</option>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label for="km">QUILOMETRAGEM :</label>
                        <input id="km" type="number" autocomplete="off" class="form-control" name="km" value="" min="0" required/>
                    </div>

                    <div class="col-md-4">
                        <label for="valor">VALOR:</label>
                        <div class="input-group">
                            <input autocomplete="off" id="valor" placeholder="" type="text" class="form-control" name="valor" value="" required />
                            <div class="input-group-append">
                                <span class="input-group-text">,00 R$</span>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function(){
                                $('#valor').mask('000000', {reverse: true});
                            });
                        </script>
                    </div>

                    <div class="col-md-12">
                        <label for="descricao">DESCRIÇÃO:</label>
                        <textarea id="descricao" onkeyup="limite_textarea(this.value,250,descricao,'cont')" maxlength="250" class="form-control" rows="1" name="descricao"></textarea>
                        <span id="cont">250</span>/250
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
                    </div>
                    <script>
                        var formID = document.getElementById("formx");
                        var send = $("#gogo");

                        $(formID).submit(function(event){
                            if (formID.checkValidity()) {
                                send.attr('disabled', 'disabled');
                                send.attr('value', 'AGUARDE...');
                            }
                        });
                    </script>
                </div>
            </form>
        </div>

    </div>


<?php
// Recebe
    $id_vi = $_GET['id'];
    //existe um id e se ele é numérico
    if (!empty($id_vi) && is_numeric($id_vi)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT * \n"
            . "FROM ztst_log_viagens_at \n"
            . "WHERE (id_viagem=? and status=1)\n"
            . "ORDER BY data_ts DESC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $id_vi);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $atividades = $consulta->fetchAll();
        $sql = null;
        $consulta = null;
    }

?>

<div class="card mt-2">
    <div id="pointofview" class="card-header bg-info text-light">
        Historico da viagem
    </div>
    <div class="card-body">
        <?php
        foreach ($atividades as $at) {
            ?>
            <hr>
            <blockquote class="blockquote blockquote-info text-uppercase">
                DATA: <strong class="text-info"><?php echo datahoraBanco2data($at['data_ts']); ?>&nbsp&nbsp</strong><br>
                TIPO DE LANÇAMENTO: <strong class="text-info">
                    <?php
                    if ($at['tipo'] == 1) {
                        echo "Carregamento";
                    }
                    if ($at['tipo'] == 2) {
                        echo "Descarregamento";
                    }
                    if ($at['tipo'] == 3) {
                        echo "Gastos";
                    }
                    if ($at['tipo'] == 4) {
                        echo "Deslocamento";
                    }
                    ?>
                </strong><br>
                KILOMETRAGEM: <strong class="text-info">
                    <?php echo $at['km']; ?>
                </strong><br>
                VALOR: <strong class="text-info">R$<?php echo number_format($at['valor'],0,',','.'); ?></strong><br>
                DESCRIÇÃO: <strong class="text-info"><?php echo $at['descricao']; ?>&nbsp&nbsp</strong><br>

                <footer class="blockquote-footer">
                    <?php
                    $us=fncgetusuario($at['usuario']);
                    echo $us['nome'];
                    ?>
                </footer>
            </blockquote>
            <?php
            echo "</h6>";
        }
        ?>
    </div>
</div>





</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>