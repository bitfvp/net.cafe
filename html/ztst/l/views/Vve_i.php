<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar at veiculo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id_veiculo']) and is_numeric($_GET['id_veiculo'])){
    $veiculo=fncgetveiculo($_GET['id_veiculo']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}

if (isset($_GET['id_item']) and is_numeric($_GET['id_item'])){
    $item=fncgetveiculo_item($_GET['id_item']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}


if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="veiculo_atupdate";
    $veiculo_at=fncgetveiculo_at($_GET['id']);

}else{
    $a="veiculo_atinsert";

}
?>
<main class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados do item
                </div>
                <div class="card-body text-uppercase">
                    <blockquote class="blockquote blockquote-info">
                        <header>TIPO: <strong class="text-info"><?php
                                if ($item['tipo'] == 1) {
                                    echo "Óleo";
                                }
                                if ($item['tipo'] == 2) {
                                    echo "Freio";
                                }
                                if ($item['tipo'] == 5) {
                                    echo "Pneu";
                                }
                                ?>&nbsp;&nbsp;</strong></header>
                        <h5>
                            QUILOMETRAGEM DA ÚLTIMA TROCA: <strong class="text-info"><?php echo $item['km_troca']; ?></strong>
                        </h5>
                        <h5>
                            QUILOMETRAGEM DA PRÓXIMA TROCA: <strong class="text-info"><?php echo $item['km_futuro']; ?></strong>
                        </h5>
                        <h5>
                            QUILOMETRAGEM ATUAL: <strong class="text-info"><?php echo $veiculo['km']; ?></strong>
                        </h5>
                    </blockquote>
                </div>
                <!-- fim da col md 4 -->
            </div>
        </div>
    </div>





    <div class="card mt-2">
        <div class="card-body">
            <form class="form-signin" action="<?php echo "index.php?pg=Vve_i&aca={$a}"; ?>" method="post" id="formx">
                <h3 class="form-cadastro-heading">Cadastrar manutenção ou troca</h3>
                <hr>
                <div class="row">
                    <input id="id_item" type="hidden" class="txt bradius" name="id_item" value="<?php echo $_GET['id_item']; ?>"/>
                    <input id="id_veiculo" type="hidden" class="txt bradius" name="id_veiculo" value="<?php echo $_GET['id_veiculo']; ?>"/>

                    <div class="col-md-4">
                        <label for="km_troca">KILOMETRAGEM :</label>
                        <input id="km_troca" type="number" autocomplete="off" class="form-control" name="km_troca" value="<?php echo $veiculo_at['km_troca']; ?>" min="0" required/>
                    </div>

                    <div class="col-md-4">
                        <label for="km_futuro">KILOMETRAGEM LIMITE:</label>
                        <input id="km_futuro" type="number" autocomplete="off" class="form-control" name="km_futuro" value="<?php echo $veiculo_at['km_futuro']; ?>" min="0" required/>
                    </div>

                    <div class="col-md-12">
                        <label for="descricao">DESCRIÇÃO:</label>
                        <textarea id="descricao" onkeyup="limite_textarea(this.value,250,descricao,'cont')" maxlength="250" class="form-control" rows="1" name="descricao"><?php echo $veiculo_at['descricao']; ?></textarea>
                        <span id="cont">250</span>/250
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
                    </div>
                    <script>
                        var formID = document.getElementById("formx");
                        var send = $("#gogo");

                        $(formID).submit(function(event){
                            if (formID.checkValidity()) {
                                send.attr('disabled', 'disabled');
                                send.attr('value', 'AGUARDE...');
                            }
                        });
                    </script>
                </div>
            </form>
        </div>

    </div>


<?php
// Recebe
    $id_veiculo = $_GET['id_veiculo'];
    $id_item = $_GET['id_item'];
    //existe um id e se ele é numérico
    if (!empty($id_item) && is_numeric($id_item)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT * \n"
            . "FROM ztst_log_veiculos_at \n"
            . "WHERE ((id_veiculo=? and id_item=?) and status=1)\n"
            . "ORDER BY data_ts DESC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $id_veiculo);
        $consulta->bindParam(2, $id_item);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $lotes = $consulta->fetchAll();
        $sql = null;
        $consulta = null;
    }

?>

<div class="card mt-2">
    <div id="pointofview" class="card-header bg-info text-light">
        Historico desse item
    </div>
    <div class="card-body">
        <?php
        foreach ($lotes as $lt) {
            ?>
            <hr>
            <blockquote class="blockquote blockquote-info text-uppercase">
                DATA: <strong class="text-info"><?php echo datahoraBanco2data($lt['data_ts']); ?>&nbsp&nbsp</strong><br>
                KILOMETRAGEM DA TROCA: <strong class="text-info">
                    <?php echo $lt['km_troca']; ?>
                </strong><br>
                KILOMETRAGEM LIMITE: <strong class="text-info">
                    <?php echo $lt['km_futuro']; ?>
                </strong><br>
                DESCRIÇÃO: <strong class="text-info"><?php echo $lt['descricao']; ?>&nbsp&nbsp</strong><br>

                <footer class="blockquote-footer">
                    <?php
                    $us=fncgetusuario($lt['usuario']);
                    echo $us['nome'];
                    ?>
                </footer>
            </blockquote>
            <?php
            echo "</h6>";
        }
        ?>
    </div>
</div>





</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>