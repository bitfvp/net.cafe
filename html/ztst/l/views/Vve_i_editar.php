<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar itens veiculo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="veiculo_itemupdate";
    $veiculo_item=fncgetveiculo_item($_GET['id']);

}else{
    $a="veiculo_iteminsert";

}
?>
<div class="container">
    <form class="form-signin" action="<?php echo "index.php?pg=Vve_i_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de itens de veiculo</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $veiculo_item['id']; ?>"/>
            <input id="id_veiculo" type="hidden" class="txt bradius" name="id_veiculo" value="<?php echo $_GET['id_veiculo']; ?>"/>
            <div class="col-md-4">
                <label for="tipo">TIPO:</label>
                <select name="tipo" id="tipo" class="form-control">
                    <option selected="" value="<?php if ($veiculo_item['tipo'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $veiculo_item['tipo'];
                    } ?>">
                        <?php
                        if ($veiculo_item['tipo'] == 0) {
                            echo "Selecione...";
                        }
                        if ($veiculo_item['tipo'] == 1) {
                            echo "Óleo";
                        }
                        if ($veiculo_item['tipo'] == 2) {
                            echo "Freio";
                        }
                        if ($veiculo_item['tipo'] == 5) {
                            echo "Pneu";
                        }
                        ?>
                    </option>
                    <option value="0">Selecione...</option>
                    <option value="1">Óleo</option>
                    <option value="2">Freio</option>
                    <option value="5">Pneu</option>
                </select>
            </div>

            <div class="col-md-4">
                <label for="linha">EIXO:</label>
                <select name="linha" id="linha" class="form-control">
                    <option selected="" value="<?php if ($veiculo_item['linha'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $veiculo_item['linha'];
                    } ?>">
                        <?php
                        if ($veiculo_item['linha'] == 0) {
                            echo "Não possui";
                        }
                        if ($veiculo_item['linha'] == 1) {
                            echo "1";
                        }
                        if ($veiculo_item['linha'] == 2) {
                            echo "2";
                        }
                        if ($veiculo_item['linha'] == 3) {
                            echo "3";
                        }
                        if ($veiculo_item['linha'] == 4) {
                            echo "4";
                        }
                        if ($veiculo_item['linha'] == 5) {
                            echo "5";
                        }
                        if ($veiculo_item['linha'] == 6) {
                            echo "6";
                        }
                        if ($veiculo_item['linha'] == 7) {
                            echo "7";
                        }
                        if ($veiculo_item['linha'] == 8) {
                            echo "8";
                        }
                        if ($veiculo_item['linha'] == 9) {
                            echo "9";
                        }

                        ?>
                    </option>
                    <option value="0">Não possui</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                </select>
            </div>


            <div class="col-md-4">
                <label for="coluna">POSIÇÃO:</label>
                <select name="coluna" id="coluna" class="form-control">
                    <option selected="" value="<?php if ($veiculo_item['coluna'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $veiculo_item['coluna'];
                    } ?>">
                        <?php
                        if ($veiculo_item['coluna'] == 0) {
                            echo "Não possui";
                        }
                        if ($veiculo_item['coluna'] == 1) {
                            echo "1";
                        }
                        if ($veiculo_item['coluna'] == 2) {
                            echo "2";
                        }
                        if ($veiculo_item['coluna'] == 3) {
                            echo "3";
                        }
                        if ($veiculo_item['coluna'] == 4) {
                            echo "4";
                        }

                        ?>
                    </option>
                    <option value="0">Não possui</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label for="km_troca">KILOMETRAGEM :</label>
                <input id="km_troca" type="number" autocomplete="off" class="form-control" name="km_troca" value="<?php echo $veiculo_item['km_troca']; ?>" min="0" required/>
            </div>

            <div class="col-md-4">
                <label for="km_futuro">KILOMETRAGEM LIMITE:</label>
                <input id="km_futuro" type="number" autocomplete="off" class="form-control" name="km_futuro" value="<?php echo $veiculo_item['km_futuro']; ?>" min="0" required/>
            </div>

            <div class="col-md-4">
                <label for="data_revisao">DATA:</label>
                <input id="data_revisao" type="date" class="form-control" name="data_revisao" value="<?php echo $veiculo_item['data_revisao']; ?>" required/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>