<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Lista de viagens-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from ztst_log_viagens WHERE data_ts LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from ztst_log_viagens ";
}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY data_ts desc LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY data_ts desc LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container">
    <h2>Listagem de viagens</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vvi_lista" hidden/>
            <input type="date" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vvi_editar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVA VIAGEM
    </a>
    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <table class="table table-striped table-hover table-sm text-uppercase">
        <thead class="thead-dark">
            <tr>
                <th>DATA</th>
                <th>CONTRATANTE</th>
                <th>VEICULO</th>
                <th>MOTORISTA</th>
                <th class="text-center">AÇÕES</th>
            </tr>
        </thead>
        <tbody>
        <?php
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $contratante = fncgetpessoa($dados['contratante']);
            $motorista = fncgetpessoa($dados['motorista']);
            $veiculo = fncgetveiculo($dados["caminhao"]) ;
            $data_ts = dataRetiraHora($dados["data_ts"]);
            ?>
            <tr>
                <td>
                    <a href="index.php?pg=Vvi&id=<?php echo $id;?>" class="text-uppercase">
                    <?php
                        echo $data_ts;
                    ?>
                    </a>
                </td>
                <td class="text-uppercase"><?php echo $contratante['nome']; ?></td>
                <td class="text-uppercase"><?php echo $veiculo['modelo']; ?></td>
                <td class="text-uppercase"><?php echo $motorista['nome']; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vvi_editar&id=<?php echo $id; ?>" title="Editar registro" class="btn btn-sm btn-primary">
                            <i class="fas fa-pen"><br>EDITAR</i>
                        </a>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
