<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $viagem=fncgetviagem($_GET['id']);
    $contratante = fncgetpessoa($viagem['contratante']);
    $motorista = fncgetpessoa($viagem['motorista']);
    $veiculo = fncgetveiculo($viagem["caminhao"]) ;
}else{
    header("Location: {$env->env_url_mod}index.php?pg=VvI_lista");
    exit();
}
?>

<div class="container">
    <?php
    include_once("../includes/ztst_cab.php");
    ?>

    <div class="row">
        <div class="col-8 text-uppercase">
            <h2>VIAGEM</h2>
            <h4>CONTRATANTE: <strong><?php echo strtoupper($contratante['nome']); ?></strong></h4>
             <h4>CAMINHÃO: <strong><?php echo $veiculo['modelo']." PLACA: ".$veiculo['placa']; ?></strong></h4>
             <h4>MOTORISTA: <strong><?php echo $motorista['nome']; ?></strong></h4>
             <h4>CARGA: <strong><?php echo $viagem['carga']." ------ VALOR COMBINADO R$:".number_format($viagem['valor'],2,',','.'); ?></strong></h4>
            <h4>SAÍDA DE: <strong><?php echo $viagem['saida']." DESTINO:".$viagem['destino']; ?></strong></h4>
             <h4>DESCRIÇÃO: <strong><?php echo $viagem['descricao']; ?></strong></h4>
        </div>
        <div class="col-4 text-right">
            <h4 class="">DATA: <strong><?php echo dataRetiraHora($viagem["data_ts"]); ?></strong></h4>
        </div>
    </div>


    <hr class="hrgrosso">
    <h3>Histórico</h3>
    <?php
    //1 carregamento
    //2 descarga
    //3 gastos
    try{
        $sql = "SELECT * \n"
            . "FROM ztst_log_viagens_at \n"
            . "WHERE (id_viagem=? and status=1)\n"
            . "ORDER BY data_ts ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['id']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $atividades = $consulta->fetchAll();
    $atividades_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-hover table-striped">
                <thead class="thead-inverse thead-dark">
                <tr>
                    <th>DATA</th>
                    <th>TIPO</th>
                    <th>VALOR</th>
                    <th>KM</th>
                    <th>DESCRIÇÃO</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $gasto=0;
                foreach ($atividades as $dados){
                    $gasto=$gasto+$dados['valor'];
                    ?>

                    <tr id="" class="">
                        <td><?php echo datahoraBanco2data($dados['data_ts']) ?></td>
                        <td><?php
                            if ($dados['tipo'] == 1) {
                                echo "Carregamento";
                            }
                            if ($dados['tipo'] == 2) {
                                echo "Descarregamento";
                            }
                            if ($dados['tipo'] == 3) {
                                echo "Gastos";
                            }
                            if ($dados['tipo'] == 4) {
                                echo "Deslocamento";
                            }
                            ?></td>
                        <td>R$ <?php echo number_format($dados['valor'],2,',','.'); ?></td>
                        <td style='white-space: nowrap;'><?php echo $dados['km']; ?></td>
                        <td><?php echo $dados['descricao']; ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php
    try{
        $sql = "SELECT MIN(km) as minimo \n"
            . "FROM ztst_log_viagens_at \n"
            . "WHERE (id_viagem=? and tipo=1 and status=1)\n"
            . "ORDER BY data_ts ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['id']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $minimo = $consulta->fetch();
    $sql = null;
    $consulta = null;

    try{
        $sql = "SELECT max(km) as maximo \n"
            . "FROM ztst_log_viagens_at \n"
            . "WHERE (id_viagem=? and tipo=2 and status=1)\n"
            . "ORDER BY data_ts ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['id']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $maximo = $consulta->fetch();
    $sql = null;
    $consulta = null;
    ?>

    <div class="row">
        <div class="col-6 border">
            GASTOS: R$ <strong><?php echo number_format($gasto,2,',','.') ?></strong>
        </div>
    </div>
    <div class="row">
        <div class="col-5 border">
            QUILOMETRAGEM DE CARREGAMENTO: <strong><?php echo $minimo['minimo']." KMs"; ?></strong>
        </div>
        <div class="col-5 border">
            QUILOMETRAGEM DE DESCARREGAMENTO: <strong><?php echo $maximo['maximo']." KMs"; ?></strong>
        </div>
        <div class="col-2 border">
            <strong><?php if ( (is_numeric($minimo['minimo']) and is_numeric($maximo['maximo'])) and ($maximo['maximo']>=$minimo['minimo']) ){ $tempkm=$maximo['maximo']-$minimo['minimo']; echo $tempkm." KMs";} ?></strong>
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Usuário: <strong><?php echo fncgetusuario($_SESSION['id'])['nome']; ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>