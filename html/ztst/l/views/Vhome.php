<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_18"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vhome'>";
include_once("includes/topo.php");

?>

<main class="container">


    <div class="row">

        <?php
        try{
            $sql="SELECT * FROM ";
            $sql.="ztst_log_veiculos ";
            $sql.="WHERE status=1 order by id asc";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $veiculos=$consulta->fetchAll();
        ?>

        <div class="col-md-12 bg-light">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Veiculos com itens que necessitam atenção
                </div>
                <div class="card-body">
                    <table class="table table-sm">
                        <thead class="thead-dark">
                        <tr>
                            <th>VEICULO</th>
                            <th>NECESSIDADE</th>
                        </tr>
                        </thead>

                        <tbody>


                    <?php
                    foreach ($veiculos as $veiculo){
                        $tempkmv=$veiculo['km']+499;
                        try{
                            $date_temp = date("Y-m-d");
                            $sql="SELECT * FROM ";
                            $sql.="ztst_log_veiculos_itens ";
                            $sql.="WHERE status=1 and id_veiculo=:id_veiculo and km_futuro<=:kilometragem order by id asc";
                            global $pdo;
                            $consulta=$pdo->prepare($sql);
                            $consulta->bindValue(":id_veiculo", $veiculo['id']);
                            $consulta->bindValue(":kilometragem", $tempkmv);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        }catch ( PDOException $error_msg){
                            echo 'Erroff'. $error_msg->getMessage();
                        }
                        $itens=$consulta->fetchAll();

                        foreach ($itens as $item){
                            $veiculo=fncgetveiculo($item['id_veiculo']);
                            if ($item['tipo']==1){
                                $temp="TROCA DE ÓLEO";
                            }
                            if ($item['tipo']==2){
                                $temp="REVISAR FREIO";
                            }
                            if ($item['tipo']==5){
                                $temp="REVISAR PNEU";
                            }
                            echo "<tr>";
                            echo "<td>";
                            echo "<a href='index.php?pg=Vve&id={$item['id_veiculo']}' class='text-uppercase text-danger'><h4>{$veiculo['modelo']}</h4></a><br>";
                            echo "</td>";

                            echo "<td>";
                            echo "<a href='index.php?pg=Vve&id={$item['id_veiculo']}' class='text-uppercase text-danger'><h4>{$temp}</h4></a><br>";
                            echo "</td>";

                            echo "</tr>";

                        }

                    }

                    ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>



</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>