<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar Abastecimento veiculo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="abastecimentoupdate";
    $abastecimento=fncgetabastecimento($_GET['id']);

}else{
    $a="abastecimentoinsert";

}
?>
<main class="container">
    <div class="row">

        <form class="form-signin" action="<?php echo "index.php?pg=Vve_a&aca={$a}"; ?>" method="post" id="formx">
            <h3 class="form-cadastro-heading">Cadastro de abastecimento</h3>
            <hr>
            <div class="row">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $abastecimento['id']; ?>"/>
                <input id="id_veiculo" type="hidden" class="txt bradius" name="id_veiculo" value="<?php echo $_GET['id_veiculo']; ?>"/>


                <div class="col-md-6">
                    <label for="id_motorista">MOTORISTA:</label>
                    <select name="id_motorista" id="id_motorista" class="form-control input-sm" data-live-search="true" required>
                        <?php
                        $getmotorista=fncgetpessoa($abastecimento['id_motorista']);
                        ?>
                        <option selected="" data-tokens="<?php echo $getmotorista['nome'];?>" value="<?php echo $abastecimento['id_motorista']; ?>">
                            <?php echo $getmotorista['nome'];?>
                        </option>
                        <?php
                        foreach (fncpessoamotoristalist() as $item) {
                            ?>
                            <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                <?php echo strtoupper($item['nome']); ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="col-md-4">
                    <label for="data">DATA:</label>
                    <input id="data" type="date" class="form-control" name="data" value="<?php echo $abastecimento['data']; ?>" required/>
                </div>

                <div class="col-md-4">
                    <label for="quantidade">QUANTIDADE DE COMBUSTIVEL :</label>
                    <input id="quantidade" type="number" autocomplete="off" class="form-control" name="quantidade" value="<?php echo $abastecimento['quantidade']; ?>" min="0" step="any" required/>
                </div>


                <div class="col-md-4">
                    <label for="valor">VALOR:</label>
                    <div class="input-group">
                        <input autocomplete="off" id="valor" placeholder="" type="number" class="form-control" name="valor" value="<?php echo $abastecimento['valor']; ?>" min="0" step="any" required />
                        <div class="input-group-append">
                            <span class="input-group-text"> R$</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <label for="km">KILOMETRAGEM :</label>
                    <input id="km" type="number" autocomplete="off" class="form-control" name="km" value="<?php echo $abastecimento['km']; ?>" min="0" required/>
                </div>


            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
                </div>
                <script>
                    var formID = document.getElementById("formx");
                    var send = $("#gogo");

                    $(formID).submit(function(event){
                        if (formID.checkValidity()) {
                            send.attr('disabled', 'disabled');
                            send.attr('value', 'AGUARDE...');
                        }
                    });
                </script>
            </div>
        </form>
    </div>



<div class="row">
        <?php
        $sql = "SELECT * "
            . "FROM ztst_log_veiculos_abastecimentos  "
            . "WHERE  id_veiculo = ? "
            ."ORDER BY "
            ."`id` DESC";
        global $pdo;
        $cons = $pdo->prepare($sql);
        $cons->bindParam(1, $_GET['id_veiculo']);
        $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
        $abastecimentos = $cons->fetchAll();
        $sql=null;
        $consulta=null;
        ?>
        <table id="tabela" class="table table-striped table-sm table-hover">
            <thead class="bg-primary">
            <tr>
                <th>MOTORISTA</th>
                <th>DATA</th>
                <th>QUANTIDADE</th>
                <th>VALOR</th>
                <th>KM</th>
                <th>EDITAR</th>
            </tr>
            </thead>
            <tbody>
            <?php
            // vamos criar a visualização
            foreach ($abastecimentos as $dados) {
                $id = $dados["id"];
                $motorista=fncgetpessoa($dados["id_motorista"]);
                $data= dataBanco2data($dados["data"]);
                $quantidade=$dados["quantidade"];
                $valor=$dados["valor"];
                $km=$dados["km"];
                ?>

                <tr>
                    <td><a href="index.php?pg=Vmotorista&id=<?php echo $motorista['id']; ?>"><?php echo $motorista['nome']; ?></a></td>
                    <td><?php echo $data; ?></td>
                    <td><?php echo $quantidade; ?> Lts</td>
                    <td>R$ <?php echo $valor; ?></td>
                    <td><?php echo $km; ?></td>
                    <td>
                            <a href="index.php?pg=Vve_a&id_veiculo=<?php echo $_GET['id_veiculo']; ?>&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a>
                        <i class="badge badge-warning float-right">
                            <strong><?php echo $id; ?></strong>
                        </i>
                    </td>
                </tr>

            <?php } ?>
            </tbody>
        </table>

</div>





</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>