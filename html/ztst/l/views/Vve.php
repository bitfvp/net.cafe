<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_18"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $veiculo=fncgetveiculo($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<main class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados do veículo
                </div>
                <div class="card-body text-uppercase">
                    <blockquote class="blockquote blockquote-info">
                        <header>MODELO: <strong class="text-info"><?php echo $veiculo['modelo']; ?>&nbsp;&nbsp;</strong></header>
                        <h5>
                            PLACA: <strong class="text-info"><?php echo $veiculo['placa']; ?></strong>
                        </h5>
                        <h5>
                            OUTROS: <strong class="text-info"><?php echo $veiculo['outros']; ?></strong>
                        </h5>
                        <h5>
                            KILOMETRAGEM: <strong class="text-info"><?php echo $veiculo['km']; ?></strong>
                        </h5>
                    </blockquote>
                    <div class="row">
                        <div class="col-md-4">
                            <a href="index.php?pg=Vve_editar&id=<?php echo $veiculo['id']; ?>" class="btn btn btn-info btn-block">
                                EDITAR VEICULO
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="index.php?pg=Vve_i_editar&id_veiculo=<?php echo $veiculo['id']; ?>" class="btn btn btn-success btn-block">
                                ADICIONAR PARTE
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="index.php?pg=Vve_a&id_veiculo=<?php echo $veiculo['id']; ?>" class="btn btn btn-success btn-block">
                                ABASTECIMENTOS
                            </a>
                        </div>
                    </div>
                </div>
                <!-- fim da col md 4 -->
            </div>
        </div>
    </div>


    <div class="row text-center">


    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=1 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $oleo_itemlista = $consulta->fetchAll();
    $oleo_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($oleo_itemlista_cont==1){
        $tamanho=12;
    }
    if ($oleo_itemlista_cont==2){
        $tamanho=6;
    }
    if ($oleo_itemlista_cont==3){
        $tamanho=4;
    }
    if ($oleo_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($oleo_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>TROCA DE ÓLEO</h2>";
        echo "</div>";
    }

    foreach ($oleo_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor}'>";
        echo " <h4>Óleo revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da troca: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima troca: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>






    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=2 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $freio_itemlista = $consulta->fetchAll();
    $freio_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($freio_itemlista_cont==1){
        $tamanho=12;
    }
    if ($freio_itemlista_cont==2){
        $tamanho=6;
    }
    if ($freio_itemlista_cont==3){
        $tamanho=4;
    }
    if ($freio_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($freio_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>REVISÃO DO FREIO</h2>";
        echo "</div>";
    }


    foreach ($freio_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor}'>";
        echo " <h4>Freio revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>




    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=1 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 1° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>


    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=2 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 2° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>


    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=3 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 3° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>



    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=4 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 4° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>



    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=5 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 5° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>


    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=6 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 6° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>



    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=7 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 7° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>

    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=8 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 8° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>


    <?php
    $sql = "SELECT * FROM ztst_log_veiculos_itens where id_veiculo={$_GET['id']} and tipo=5 and linha=9 and status=1 ORDER BY coluna";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pneu_itemlista = $consulta->fetchAll();
    $pneu_itemlista_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    if ($pneu_itemlista_cont==1){
        $tamanho=12;
    }
    if ($pneu_itemlista_cont==2){
        $tamanho=6;
    }
    if ($pneu_itemlista_cont==3){
        $tamanho=4;
    }
    if ($pneu_itemlista_cont>=4){
        $tamanho=3;
    }

    if ($pneu_itemlista_cont>0){
        echo "<div class='col-md-12 text-white bg-dark'>";
        echo "<h2>PNEUS NO 9° EIXO</h2>";
        echo "</div>";
    }


    foreach ($pneu_itemlista as $item){
        $temp1=$item['km_futuro']-$item['km_troca'];
        $tempporcento=$temp1/100;
        $temp10=$tempporcento*10;
//        $veiculo['km']
        $falta=$item['km_futuro']-$veiculo['km'];

        if ($falta<$temp10){
            $cor="bg-warning";
            $faltatexto="Falta apenas ".$falta."KMs";
        }
        if ($falta>=$temp10){
            $cor="";
            $faltatexto=$falta."KMs para próxima revisão";
        }
        if ($falta<=0){
            $cor="bg-danger";
            $faltatexto="Venceu";
        }

        echo "<div class='col-md-{$tamanho} {$cor} border'>";
        echo " <h4>Pneu revisado em: ".dataBanco2data($item['data_revisao'])."</h4>";
        echo " <h4>Kilometragem da revisão: ".$item['km_troca']." KMs</h4>";
        echo " <h4>Próxima revisão: ".$item['km_futuro']."KMs</h4>";
        echo " <h4><i class='blink'>".$faltatexto."</i></h4>";

        echo "<div class='btn-group mb-2' role='group' aria-label=''>";
        echo "<a href='index.php?pg=Vve_i&id_veiculo={$item['id_veiculo']}&id_item={$item['id']}' title='acessar' class='btn btn-sm btn-success fas fa-search-plus text-dark'><br>ACESSAR</a>";
        echo "<a href='index.php?pg=Vve_i_editar&id_veiculo={$item['id_veiculo']}&id={$item['id']}' title='Click para alterar para sim' class='btn btn-info'>EDITAR</a>";
        echo "</div>";

        echo "</div>";
    }
    ?>

    </div>
</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>