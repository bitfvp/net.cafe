<div id="clock" class="finalcountdown bg-info text-danger text-center pt-3" title="Sessão Expirá Em"></div>
<?php
echo '<script src="' .$env->env_estatico . 'js/jquery.countdown.min.js"></script>' . "\n";
$horamais1 = date('H');
$diamais1 = date('d');
if ($horamais1==23){
    $horamais1="00";
    $diamais1++;
}else{
    $horamais1++;
}
?>
<script type="text/javascript">
    $(function(){
        $("#clock").hide();
    });

    var i = 2;
    setInterval(function(){
        $("#clock").fadeIn(2000).delay(8000);
    }, 2400000);

    //mostrar contagem regressiva
    $(document).ready(function() {
        $('#clock').countdown('<?php echo date('Y')."/".date('m')."/".$diamais1." ".$horamais1.":".date('i').":".date('s');?>', function(event) {
            $(this).html(event.strftime(/*%D dias %H:*/ '%M:%S'));
        });
    });
</script>